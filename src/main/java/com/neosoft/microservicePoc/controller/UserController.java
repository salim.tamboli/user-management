package com.neosoft.microservicePoc.controller;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.neosoft.microservicePoc.model.User;
import com.neosoft.microservicePoc.repository.UserRepository;
import com.neosoft.microservicePoc.serviceInterface.UserServiceInterface;
import com.neosoft.microservicePoc.valueObject.ResposeTemplateVO;



@RestController
@RequestMapping("/users")
public class UserController {
	
	@Autowired
	private UserServiceInterface userServiceInterface;

		
	@PostMapping("/saveUser")
	public ResponseEntity<?> addUser(@RequestBody User user)
	{		
		if(user!=null)
		{
		userServiceInterface.addUser(user);	
		return new ResponseEntity<String>("user saved successfully",HttpStatus.CREATED);
		}
		else
		{
			return new ResponseEntity<String>("user not saved please try again",HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/getUser")
	public ResponseEntity<?> getUser()
	{
		List<User> list=userServiceInterface.getUser();
		if(list!=null)
		{
		return new ResponseEntity<List<User>>(list,HttpStatus.FOUND);
		}
		else
		{
			return new ResponseEntity<List<User>>(list,HttpStatus.NOT_FOUND);
		}
	}

	
	@GetMapping("/all/{userId}")
	public ResponseEntity<?> getUserWithDepartment(@PathVariable("userId") int userId) {		

		ResposeTemplateVO vo= userServiceInterface.getUserWithDepartment(userId);
		if(vo!=null)
		{
				return new ResponseEntity<ResposeTemplateVO>(vo, HttpStatus.FOUND);	
		}
		else
		{
			return new ResponseEntity<ResposeTemplateVO>(vo, HttpStatus.NOT_FOUND);
		}
	
	}
	
	@DeleteMapping("/deleteUser/{userId}")
	public ResponseEntity<?> DeleteUserById(@PathVariable("userId") int userId) {		
				if(userId==0)
				{
				 userServiceInterface.deleteUser(userId);
				return new ResponseEntity<User>(HttpStatus.FOUND);	
				}
				else
				{
					return new ResponseEntity<User>(HttpStatus.NOT_FOUND);	
				}
	
	}
	
	
	@PutMapping("/updateUser/{userId}")
	public ResponseEntity<?> UpdateUserById(@RequestBody User user, @PathVariable("userId") int userId) {
	
			if (user != null) {

				User savedUser = userServiceInterface.updateUser(user, userId);
				return new ResponseEntity<User>(savedUser, HttpStatus.CREATED);
			} else {

				return new ResponseEntity<User>(HttpStatus.NOT_MODIFIED);
			}
		
	
	}
}