package com.neosoft.microservicePoc.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.neosoft.microservicePoc.model.User;
import com.neosoft.microservicePoc.repository.UserRepository;
import com.neosoft.microservicePoc.serviceInterface.UserServiceInterface;
import com.neosoft.microservicePoc.valueObject.Department;
import com.neosoft.microservicePoc.valueObject.ResposeTemplateVO;

@Service
public class UserServiceImplementation implements UserServiceInterface {

	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Override
	public String addUser(User user) {
	userRepository.save(user);
		return "save";
	}

	@Override
	public List<User> getUser() {
		List<User> list=userRepository.findAll();
		return list;
		

	}

	@Override
	public void deleteUser(int id) {
		userRepository.deleteById(id);
		
	}

	@Override
	public ResposeTemplateVO getUserWithDepartment(int id) {
		ResposeTemplateVO vo=new ResposeTemplateVO();
		User user=userRepository.findByUserId(id);
		Department dept=restTemplate.getForObject("http://DEPT-SERVICE/departments/"+user.getDeptId(), Department.class);
		
		vo.setUser(user);
		vo.setDepartment(dept);
		
		return vo;
	}

	@Override
	public User updateUser(User user, int userId) {
		User updateUser=userRepository.save(user);
		return updateUser;
	}
	
	
}
