package com.neosoft.microservicePoc.serviceInterface;

import java.util.List;

import com.neosoft.microservicePoc.model.User;
import com.neosoft.microservicePoc.valueObject.ResposeTemplateVO;

public interface UserServiceInterface {

	String addUser(User user);

	List<User> getUser();

	void deleteUser(int id);

	ResposeTemplateVO getUserWithDepartment(int id);

 User updateUser(User user, int userId);
}
