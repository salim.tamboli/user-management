package com.neosoft.microservicePoc.valueObject;

import com.neosoft.microservicePoc.model.User;

public class ResposeTemplateVO {
	
	private User user;
	private Department department;
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Department getDepartment() {
		return department;
	}
	public void setDepartment(Department department) {
		this.department = department;
	}

}
