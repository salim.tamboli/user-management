package com.neosoft.microservicePoc.valueObject;

public class Department {
	private int deptId;
	private String deptName;
	private String deptAddress;
	private String deptPassword;
	private String deptUsername;
	public int getDeptId() {
		return deptId;
	}
	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public String getDeptAddress() {
		return deptAddress;
	}
	public void setDeptAddress(String deptAddress) {
		this.deptAddress = deptAddress;
	}
	public String getDeptPassword() {
		return deptPassword;
	}
	public void setDeptPassword(String deptPassword) {
		this.deptPassword = deptPassword;
	}
	public String getDeptUsername() {
		return deptUsername;
	}
	public void setDeptUsername(String deptUsername) {
		this.deptUsername = deptUsername;
	}

}
