package com.neosoft.microservicePoc.repository;



import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.neosoft.microservicePoc.model.User;

@Repository
public interface UserRepository extends MongoRepository<User, Integer>{

	User findByUserId(int id);

}
